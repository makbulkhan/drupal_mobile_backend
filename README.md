# Drupal as backend for mobile apps

Source files for the [Drupal as Backend for Mobile Apps](https://bitbucket.org/chandima/drupal_mobile_backend/downloads/Drupal_as_Mobile_Backend.pdf) Presentation.

This is an HTML, CSS and JS app build iteratively in 11 steps that is meant to be the source for a hybrid mobile application compiled using [Apache Cordova](http://cordova.apache.org). The app consumes data from the [Birds Drupal site](https://bitbucket.org/chandima/birds). 

Clone the Birds Drupal site from <https://bitbucket.org/chandima/birds>. The DB for the Birds site is available in the [downloads](https://bitbucket.org/chandima/birds/downloads) section and admin credentials are in the README file.

Change the *bird_server* varaiable in the Javascript to match your custom Birds site.

###Presentation PDF
[Drupal_as_Mobile_Backend.pdf](https://bitbucket.org/chandima/drupal_mobile_backend/downloads/Drupal_as_Mobile_Backend.pdf)